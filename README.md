# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Computer Security Course Faculty of computer and information sciences Ain Shams University.

Audience: Fourth Year Students [CSYS – CS - IS]

### How do I get set up? 

The solution you have consist of 2 projects:
    “SecurityLibrary”: a dll project in which you’ll write all your code.
    “SecurityPackageTest”: a unit test project that you’ll use to test your project. 

The “SecurityLibrary” project consists of a class for each algorithm. You have to remove the thrown exception and write your code in the correct place. Feel free to add the functions you need, you just need to keep the signature of these functions as they are:
      public string Encrypt(string plainText, int key)
      public string Decrypt(string cipherText, int key)
      public int Analyse(string plainText, string cipherText)
        
To test your code:
       Build the solution.
       Open test explorer (Test -> Windows -> Test explorer)
       If you want to run:
              All tests -> “Run all”
              A specific test -> right click, Run selected test
              The tests of a specific algorithm -> open the test class of this algorithm, right click, run tests

For algorithms 9-14: 
]         Go to the test file of the algorithms you chose and remove [Ignore] from the class.


Additional test cases will be added, so make sure you’re coding the algorithms correctly.